fbprophet==0.6
numpy==1.18.1
altair==3.2.0
pandas==1.0.1
plotly==4.5.2
ipython==7.14.0
