import pandas as pd
import numpy as np
from fbprophet import Prophet
import plotly.express as px
import altair as alt
import time
from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = "all"


def groupby(data, categories=[],inpatient=False):
    filters = ['periodStart']
    filters.extend(categories)
    if inpatient == False:
        data = data[data['claimCategory'] == 'OUTPATIENT']
    else:
        data = data[data['claimCategory'] == 'INPATIENT']
    agg = data.groupby(filters,as_index=False).sum()
    return agg



def grouped2df(data,metric='numClaims'):
    df = pd.DataFrame({'ds':data['periodStart'],'y':data[metric]})
    return df



def forecast(data,confidence=0.99):
    data['floor'] = 0
    model = Prophet(interval_width=confidence)
    model.fit(data)
    future_data = model.make_future_dataframe(periods=52)
    future_data['floor'] = 0
    forecast = model.predict(future_data)
    fig2 = model.plot_components(forecast)
    forecast['fact'] = data['y'].reset_index(drop = True)
    return forecast

def detect_anomalies(forecast):
    forecasted = forecast[['ds','trend', 'yhat', 'yhat_lower', 'yhat_upper', 'fact']].copy()
    forecasted['anomaly'] = 0
    forecasted.loc[forecasted['fact'] > forecasted['yhat_upper'], 'anomaly'] = 1
    forecasted.loc[forecasted['fact'] < forecasted['yhat_lower'], 'anomaly'] = -1
    forecasted['importance'] = 0
    forecasted.loc[forecasted['anomaly'] ==1, 'importance'] = (forecasted['fact'] - forecasted['yhat_upper'])/forecast['fact']
    forecasted.loc[forecasted['anomaly'] ==-1, 'importance'] = (forecasted['yhat_lower'] - forecasted['fact'])/forecast['fact']
    return forecasted
def plot_anoms(forecasted,title='Anomaly Detection',ytitle='claims'):
    interval = alt.Chart(forecasted).mark_area(interpolate="basis", color = '#e6f5f1').encode(
    x=alt.X('ds:T',  title ='date'),
    y='yhat_upper',
    y2='yhat_lower',
    tooltip=['ds', 'fact', 'yhat_lower', 'yhat_upper']
    ).interactive().properties(
        title=title
    )
    band = alt.Chart(forecasted).mark_line(interpolate="basis", color = '#7fc9b5').encode(
    x=alt.X('ds:T',  title ='date'),
    #y='yhat_upper',
    y='yhat',
    tooltip=['ds', 'fact', 'yhat_lower', 'yhat_upper']
    ).interactive().properties(
        title=title
    )
    fact = alt.Chart(forecasted[forecasted.anomaly==0]).mark_circle(size=15, opacity=0.7, color = 'Black').encode(
        x='ds:T',
        y=alt.Y('fact', title=ytitle),
        tooltip=['ds', 'fact', 'yhat_lower', 'yhat_upper']
    ).interactive()

    anomalies = alt.Chart(forecasted[forecasted.anomaly!=0]).mark_circle(size=45, color = 'Red').encode(
        x='ds:T',
        y=alt.Y('fact', title=ytitle),
        tooltip=['ds', 'fact', 'yhat_lower', 'yhat_upper']
        #,size = alt.Size( 'importance', legend=None)
    ).interactive()

    return alt.layer(interval, band,fact, anomalies).properties(width=900, height=400).configure_title(fontSize=20)

def plot_metrics(forecasted,title='Anomaly Detection',ytitle='claims'):
    
    highlight = alt.selection(type='single', on='mouseover',
                          fields=['variable'], nearest=True, empty="none")

    nearest = alt.selection(type='single', nearest=True, on='mouseover',
                        fields=['ds'], empty='none')
    scale = alt.Scale(domain=['sun', 'fog', 'drizzle', 'rain', 'snow'],
                  range=['#e7ba52', '#c7c7c7', '#aec7e8', '#1f77b4', '#9467bd'])
    pred = alt.Chart(forecasted).mark_line(interpolate="basis", color = '#69D2E7').encode(
    x=alt.X('ds:T',  title ='date'),
    #y='yhat_upper',
    y='yhat',
    color='Category',
    tooltip=['ds', 'fact', 'yhat','yhat_lower', 'yhat_upper','Category']
    ).add_selection(highlight).interactive().properties(
        title=title
    )
    interval = alt.Chart(forecasted).mark_area(interpolate="basis",opacity=0.5).encode(
    x=alt.X('ds:T',  title ='date'),
    y='yhat_upper',
    y2='yhat_lower',
    color='Category',
    tooltip=['ds', 'fact','yhat','yhat_lower', 'yhat_upper','Category']
    ).interactive().properties(
        title=title,
    )
    fact = alt.Chart(forecasted[forecasted.anomaly==0]).mark_circle(size=15, opacity=0.9, color = '#3dc5e0').encode(
        x='ds:T',
        y=alt.Y('fact', title=ytitle),
        tooltip=['ds', 'fact','yhat', 'yhat_lower', 'yhat_upper','Category']
    ).interactive()

    anomalies = alt.Chart(forecasted[forecasted.anomaly!=0]).mark_point(size=100, color = '#004d99',shape='cross',filled=True).encode(
        x='ds:T',
        y=alt.Y('fact', title=ytitle),
        color='Category',
       # shape=alt.Shape('cross'),
        tooltip=['ds', 'fact','yhat', 'yhat_lower', 'yhat_upper','Category']
        #,size = alt.Size( 'importance', legend=None)
    ).interactive()


    return alt.layer(pred,interval,fact,anomalies).properties(width=900, height=400).configure_title(fontSize=20).configure(background='#f9f9f9').configure_legend(titleColor='black', titleFontSize=25,labelFontSize=15)
def data2outputs(df, metric='claims'):
    start=time.time()
    df_forecast = detect_anomalies(forecast(df))
    plot = plot_anoms(df_forecast,ytitle=metric)
    end = time.time()
    print(end-start)
    return plot

# ### Data to Forecast pipeline
# Given the data is in the correct format for the Prophet library (e.g time in `ds` column and values in `y` column) we do the following steps:
# 1. Run the forecast algorithm that returns the components of the additive model (e.g seasonality/trend/white noise)
# 2. Run the anomaly detection algorithm on the predicted values (e.g which values are drastically away from the y_lower/y_upper confidence intervals)
# 3. Run the plotting process that aims to clearly show the outliers in a visual way

# data = ... Load Data
# df3 = groupby(data,['a','b','c']) Pick Categories (e.g Specialty/DoctorNames/Providers)
# df3_unit = df3[condition] Pick entities (e.g WHAT specialty/doctor/provider (specific))
# plot_df3 = data2outputs(df3_unit,metric) Pick Metric (e.g What Y-axis value you want)
data = pd.read_csv('/data/datascience/timeseries/ts_category.csv',sep='\t')
df = groupby(data)
df2 = groupby(data,inpatient=True)
df3 = groupby(data,['doctorSpecialty'])
df_gp = df3[df3['doctorSpecialty'] == 'General Medical Practice']

# Different y metrics
df_lines = grouped2df(df,metric='numLineItems')
df_claims = grouped2df(df,metric='numClaims')
df_patients = grouped2df(df,metric='numPatients')
df_paid = grouped2df(df,metric='paidAmount')

df_gp_lines = grouped2df(df_gp,metric='numLineItems')
df_gp_claims = grouped2df(df_gp,metric='numClaims')
df_gp_patients = grouped2df(df_gp,metric='numPatients')
df_gp_paid  = grouped2df(df_gp,metric='paidAmount')

plot_lines = data2outputs(df_lines,metric='Line Items')
plot_claims = data2outputs(df_claims,metric='Claims')
plot_patients = data2outputs(df_patients,metric='Number of Patients')
plot_paid = data2outputs(df_paid,metric='Paid Amount')

gp = ' for General Practice'
plot_gp_lines = data2outputs(df_gp_lines,metric='Line Items'+gp)
plot_gp_claims = data2outputs(df_gp_claims,metric='Claims'+gp)
plot_gp_patients = data2outputs(df_gp_patients,metric='Number of Patients'+gp)
plot_gp_paid = data2outputs(df_gp_paid,metric='Paid Amount'+gp)

dic = {
    'providerName' : ['Hello','Blue'],
    'doctorName' : ['World'],
    'doctorSpecialty' : ['Bleep'],
}

dic['providerName'].append('World')
dic

def groupby(data, categories=[],inpatient=False):
    filters = ['periodStart']
    filters.extend(categories)
    if inpatient == False:
        data = data[data['claimCategory'] == 'OUTPATIENT']
    else:
        data = data[data['claimCategory'] == 'INPATIENT']
    agg = data.groupby(filters,as_index=False).sum()
    return agg

def parse_filters(data, filters):
    df = pd.DataFrame()
    for col_filter,row_filter in filters.items():
        if row_filter == []:
            continue
        elif len(df) != 0:
            df = df[df[col_filter].isin(row_filter)]
        else:
            df = data[data[col_filter].isin(row_filter)]
    df_grouped = df[['periodStart','numLineItems','numClaims','numPatients','paidAmount']]
    df_grouped = df_grouped.groupby(['periodStart'],as_index=False).sum()
    return df,df_grouped


#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
