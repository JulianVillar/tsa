# Time Series Analysis

Initial steps to use Prophet to get a feel for the types of Time Series we see across different Data Sets but also different Specialties/Providers.

Instructions to access:

1. Clone repo
2. Create python environment `conda create -n timeseries python=3.7` (note: has to be Python 3.7)
3. `pip install -r requirements.txt`
4. `pip install jupyter` (if not already done)
5. Configure port forwarding for Jupyter in ssh config, default port for Jupyter is 8888 e.g

```
		host <server_name>
    		HostName ip.address.goes.here
   		    LocalForward 11111 localhost:8888
  		    user <name>
```
